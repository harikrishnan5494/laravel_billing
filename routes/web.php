<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Auth::routes();

Route::group(['middleware' => 'auth'], function () {
// All my routes that needs a logged in user

//Route::get('/','DashboardController@index');

Route::get('/dashboard','DashboardController@index')->name('dashboard');

Route::get('/create_customer','CustomerController@create_customer')->name('create_customer');
Route::post('/store_customer','CustomerController@store_customer')->name('store_customer');
Route::get('/view_customer','CustomerController@view_customer')->name('view_customer');
Route::get('/edit_customer/{id}','CustomerController@edit_customer')->name('edit_customer');
Route::post('/update_customer/{id}','CustomerController@update_customer')->name('update_customer');
Route::delete('/delete_customer/{id}','CustomerController@delete_customer')->name('delete_customer');

Route::get('/create_vendor','VendorController@create_vendor')->name('create_vendor');
Route::post('/store_vendor','VendorController@store_vendor')->name('store_vendor');
Route::get('/view_vendor','VendorController@view_vendor')->name('view_vendor');
Route::get('/edit_vendor/{id}','VendorController@edit_vendor')->name('edit_vendor');
Route::post('/update_vendor/{id}','VendorController@update_vendor')->name('update_vendor');
Route::delete('/delete_vendor/{id}','VendorController@delete_vendor')->name('delete_vendor');


Route::get('/add_product','ProductController@add_product')->name('add_product');
Route::post('/store_product','ProductController@store_product')->name('store_product');
Route::get('/view_product','ProductController@view_product')->name('view_product');
Route::get('/edit_product/{id}','ProductController@edit_product')->name('edit_product');
Route::post('/update_product/{id}','ProductController@update_product')->name('update_product');
Route::delete('/delete_product/{id}','ProductController@delete_product')->name('delete_product');


Route::get('/add_purchase','PurchaseController@add_purchase')->name('add_purchase');
Route::post('/add_purchase','PurchaseController@add_purchase')->name('add_purchase');
Route::post('/get_vendor','AjaxController@get_vendor')->name('get_vendor');
Route::post('/get_product','AjaxController@get_product')->name('get_product');

Route::get('/create_employee','EmployeeController@create_employee')->name('create_employee');
Route::post('/store_employee','EmployeeController@store_employee')->name('store_employee');
Route::get('/view_employee','EmployeeController@view_employee')->name('view_employee');
Route::get('/edit_employee/{id}','EmployeeController@edit_employee')->name('edit_employee');
Route::post('/update_employee/{id}','EmployeeController@update_employee')->name('update_employee');
Route::delete('/delete_employee/{id}','EmployeeController@delete_employee')->name('delete_employee');

});