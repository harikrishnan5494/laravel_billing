<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Admin</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel='shortcut icon' type='image/x-icon' href='{{ asset('assets/img/favicon.ico') }}' />
</head>
<body>
<div class="loader"></div>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        @include('layouts.header');
        @include('layouts.navbar');
        <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <!--<div class="section-header">
                    <h4>ADD CUSTOMER</h4>
                </div>-->
                <div class="section-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                            <div class="card">
                                <span class="text-center">
                                    @if(session('SuccessMsg'))
                                        <script type="text/javascript">
                                            setTimeout(function(){swal("Success", "Updated Successfully!", "success");},1000);
                                            setTimeout(function(){location="{{route('view_customer')}}"},2000);
                                        </script>
                                    @endif
                                </span>
                                <form class="needs-validation" method="post" action="{{ route('update_customer',$Customer->id) }}" novalidate="">
                                    @csrf
                                    <div class="card-header">
                                        <h4>EDIT CUSTOMER</h4>
                                        <div class=" card-body text-right">
                                            <a href="{{route('view_customer')}}" class="btn btn-primary"><i class="fas fa-eye"></i> View Customer</a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Customer ID</label>
                                                    <input type="text" name="custid" class="form-control" value="{{$Customer->customer_id}}" >
                                                    <div class="invalid-feedback">
                                                        Enter The Customer ID!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Customer Name</label>
                                                    <input type="text" name="cname" value="{{$Customer->cname}}" class="form-control" required="">
                                                    <div class="invalid-feedback">
                                                        Enter The Customer Name!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Company Name</label>
                                                    <input type="text" name="cmpname" value="{{$Customer->cmpname}}" class="form-control" required="">
                                                    <div class="invalid-feedback">
                                                        Enter The Company Name!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Phone No</label>
                                                    <input type="text" name="mobile" value="{{$Customer->mobile}}" class="form-control" required="">
                                                    <div class="invalid-feedback">
                                                        Enter The Phone No!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="email" name="email" value="{{$Customer->email}}" class="form-control" required="">
                                                    <div class="invalid-feedback">
                                                        Enter Email ID!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>GST Type</label>
                                                    <select name="gst_type" class="form-control" required="">
                                                        <option value="">--Select Here--</option>
                                                        <option value="Local" {{ ( $Customer->gst_type=='Local') ? 'selected' : '' }}>Local</option>
                                                        <option value="Interstate" {{ ( $Customer->gst_type=='Interstate') ? 'selected' : '' }}>Interstate</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        Enter GST Type
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea  class="form-control" name="address" required="">{{$Customer->address}}</textarea>
                                                    <div class="invalid-feedback">
                                                        Enter The Address!
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-right">
                                        <button class="btn btn-primary" name="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    @include('layouts.footer');
</div>
<!-- General JS Scripts -->
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<!-- JS Libraies -->
<!-- Page Specific JS File -->
<!-- Template JS File -->

<script src="{{ asset('assets/bundles/sweetalert/sweetalert.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/sweetalert.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<!-- Custom JS File -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>
