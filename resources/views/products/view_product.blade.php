<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Ality - Admin Dashboard Template</title>
    <link rel="stylesheet" href="{{asset('assets/css/app.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bundles/datatables/datatables.min.css')}}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/components.css')}}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <link rel='shortcut icon' type='image/x-icon' href='{{asset('assets/img/favicon.ico')}}' />
</head>
<body>
<div class="loader"></div>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        @include('layouts.header');
        @include('layouts.navbar');
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <!--<div class="section-header">
                    <h4>ADD CUSTOMER</h4>
                </div>-->
                <div class="section-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>VIEW PRODUCT</h4>
                                    <div class=" card-body text-right">
                                        <a href="#" class="btn btn-primary"><i class="fas fa-plus"></i> Add Product</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="save-stage" style="width:100%;">
                                                <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Product ID</th>
                                                    <th>Product Name</th>
                                                    <th>Measurement</th>
                                                    <th>HSNcode</th>
                                                    <th>GST%</th>
                                                    <th>Rate</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i=1; ?>
                                                @foreach($Products as $products)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $products->product_id }}</td>
                                                    <td>{{ $products->productname }}</td>
                                                    <td>{{ $products->measurement }}</td>
                                                    <td>{{ $products->hsncode }}</td>
                                                    <td>{{ $products->gst }}</td>
                                                    <td>{{ $products->rate }}</td>
                                                    <td><a href="{{route('edit_product',$products->id)}}"><i class="fas fa-edit btn btn-info"></i></a></td>
                                                    <td><form method="post" id="delete-form-{{$products->id}}" style="display: none" action="{{route('delete_product',$products->id)}}">
                                                            @csrf
                                                            {{method_field('delete')}}
                                                        </form>
                                                        <button onclick="if(confirm('Are You Sure To Delete This Data?')){
                                                            event.preventDefault();
                                                            document.getElementById('delete-form-{{$products->id}}').submit();
                                                        }else{
                                                        event.preventDefault();
                                                        }" class=" btn btn-danger"><i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    @include('layouts.footer');
</div>

<script src="{{asset('assets/js/app.min.js')}}"></script>
<script src="{{asset('assets/bundles/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/bundles/datatables/datatables.min.js')}}"></script>


<script src="{{asset('assets/js/page/datatables.js')}}"></script>
<!-- JS Libraies -->
<!-- Page Specific JS File -->

<!-- Template JS File -->
<script src="{{asset('assets/js/scripts.js')}}"></script>
<!-- Custom JS File -->
<script src="{{asset('assets/js/custom.js')}}"></script>
</body>
</html>
