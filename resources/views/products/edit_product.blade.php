<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Purchase</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel='shortcut icon' type='image/x-icon' href="{{ asset('assets/img/favi.png') }}" />
    <!--data table-->
    <link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-daterangepicker/daterangepicker.css') }}">    
</head>
<body class="sidebar-mini">
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <!-- Header Menu Start Here -->
        @include('layouts.header');
        @include('layouts.navbar');
        <!-- Side Menu End Here -->
        <!-- Main Content -->

        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Edit Product</h1>
                </div>
                <div class="card">
                    <div class="row">
                    </div>
                    <div class="card-body">
                        <div class="card-footer text-right">
                            <a href="#"><button class="btn btn-info"><i class="fa fa-eye"></i> View Product</button></a>
                        </div>
                        <form action="{{route('update_product',$Products->id)}}" method="POST" autocomplete="off">
                            @csrf                            
                            <!-- div start here for array -->
                            <div class="row purc" >
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <div class="box-body">
                                        <span class="text-center">
                                            @if(session('SuccessMsg'))
                                                <script type="text/javascript">
                                                    setTimeout(function(){swal("Success", "Inserted Successfully!", "success");},1000);
                                                    setTimeout(function(){location="{{route('view_product')}}"},2000);
                                                </script>
                                            @endif
                                        </span>
                                            <fieldset class="row2">
                                                <p>
                                                    <button type="button" class="btn btn-danger" id="dlt-btn" onClick="deleteRow('dataTable')"><i class="fas fa-minus"></i></button>
                                                <p>(Remove option apply only to entries with check marked check boxes only.)</p>
                                                </p>
                                                <!--------------------------1st array start here ---------------------------------------------------------->
                                                <table  class="table table-bordered table-striped" id="datatable">
                                                    <thead>
                                                    <tr>
                                                        <th>Product Details</th>
                                                        <th>Measurement</th>
                                                        <th>HSN Code</th>
                                                        <th>Tax %</th>
                                                        <th>sales Rate</th>
                                                        <th></th>
                                                        <th>Add</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="dataTable">
                                                    <tr>
                                                        <p>
                                                        <td>
                                                            <input type="text" name="productname" id="productname" value="{{$Products->productname}}" class="form-control fsze" placeholder="Product Name" style="width:300px;" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="measurement" id="measurement" value="{{$Products->measurement}}" class="form-control fsze" style="width:110px;" placeholder="Measurement" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="hsncode" id="hsncode" value="{{$Products->hsncode}}" class="form-control fsze" style="width:110px;" placeholder="HSN code" required>
                                                        </td> 
                                                        <td>
                                                            <input type="text" name="gst" id="gst" class="form-control fsze" value="{{$Products->gst}}" style="width:90px;" placeholder="GST%" required>
                                                        </td>                                                        
                                                        <td>
                                                            <input type="text" name="salerate" id="salerate" value="{{$Products->rate}}" class="form-control fsze" style="width:110px;" placeholder="Sale rate">
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="productid" id="productid" class="form-control" value="{{$Products->product_id}}" style="width:98px;" placeholder="">
                                                        </td>
                                                        <td>
                                                            <button type="button" id="addbtn1" class="btn btn-success" onClick="addRow('dataTable')"><i class="fas fa-plus"></i></button>
                                                        </td>
                                                        </p>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                            <div class="card-footer text-right">
                                                <input  class="btn btn-success btn-lg" id="test" type="submit" name="save" value="Save">
                                                <input class="btn btn-danger btn-lg" id="test" type="reset" name="reset" value="Reset">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </section>
        </div>
        <!-- Footer Start Here -->
        @include('layouts.footer');
        <!-- Footer End Here -->
    </div>
</div>
<!-- Script Start Here -->
<!-- General JS Scripts -->
<script src="{{ asset('assets/js/app.min.js')}}"></script>
  <!-- JS Libraies -->
  <script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js')}}"></script>
  <!-- Page Specific JS File
  <script src="assets/js/page/index2.js"></script> -->
  <script src="{{ asset('assets/bundles/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{ asset('assets/bundles/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
  <!-- Template JS File -->
  <script src="{{ asset('assets/js/scripts.js')}}"></script>
  <!-- Custom JS File -->
  <script src="{{ asset('assets/js/custom.js')}}"></script>
<!-- Script End Here -->
</body>
</html>