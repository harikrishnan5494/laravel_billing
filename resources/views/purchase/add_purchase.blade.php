<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Purchase</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel='shortcut icon' type='image/x-icon' href="{{ asset('assets/img/favi.png') }}" />
    <!--data table-->
    <link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-daterangepicker/daterangepicker.css') }}">    
</head>
<body class="sidebar-mini">
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <!-- Header Menu Start Here -->
        @include('layouts.header');
        @include('layouts.navbar');
        <!-- Side Menu End Here -->
        <!-- Main Content -->

        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Add Purchase</h1>
                </div>
                <div class="card">
                    <div class="row">
                    </div>
                    <div class="card-body">
                        <div class="card-footer text-right">
                            <a href="#"><button class="btn btn-info"><i class="fa fa-eye"></i> View Purchase</button></a>
                        </div>
                        <form action="" method="POST" autocomplete="off">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="billno">Bill NO.</label>
                                    <input type="text" value="" class="form-control fsze" name="billno" id="billno" placeholder="Bill No." required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="billdate">Bill Date</label>
                                    <input type="text" value="" class="form-control fsze datepicker" name="billdate" id="billdate" placeholder="Bill Date" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="bill_from">Bill From<span class="text-danger"> *</span></label>
                                    <input list="bill_from" name="bill_from" id="billfrom" class="form-control fsze" placeholder="Vendor Name" required>
                                    <datalist id="bill_from" class="fsze"  required>
                                        <option value="--Select Here--" class="fsze"> </option>
                                        @foreach($Vendors as $vendors)
                                        <option class="fsze" value="{{$vendors->vname}}" data-vid="{{$vendors->vendor_id}}"> 
                                        @endforeach                                       
                                    </datalist>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4" class="control-label">Mobile No<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control numtxt" maxlength="10" name="mobile" id="mobile" placeholder="Enter Mobile No" required="">
                                    <div class="invalid-feedback">
                                        Please Enter Mobile No
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputAddress" class="control-label">Address</label>
                                    <textarea class="form-control" name="address" id="address" placeholder="Enter Address"></textarea>
                                </div>
                                <div class="form-group col-md-3">
                                    <label  class="control-label">GST Type</label>
                                    <select name="gst_type" id="gst_type" class="form-control" required="">
                                        <option value="">--Select Here--</option>
                                        <option value="Local">Local</option>
                                        <option value="Interstate">Interstate</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Enter GST Type
                                    </div>
                                </div>
                                <input type="hidden" name="vendorid" id="vendorid" class="form-control">
                                <div class="form-group col-md-3">
                                    <label for="billamt">Bill Amount</label>
                                    <input type="text" value="" class="form-control fsze"  name="billamt" id="billamt" placeholder="Bill Amount">
                                </div>
                            </div>
                            <!-- div start here for array -->
                            <div class="row purc" >
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <div class="box-body">
                                            <fieldset class="row2">
                                                <p>
                                                    <b></b>
                                                    <button type="button" class="btn btn-danger" id="dlt-btn" onClick="deleteRow('dataTable')"><i class="fas fa-minus"></i></button>
                                                <p>(Remove option apply only to entries with check marked check boxes only.)</p>
                                                </p>
                                                <!--------------------------1st array start here ---------------------------------------------------------->
                                                <table  class="table table-bordered table-striped" id="datatable">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Product Details</th>
                                                        <th>Quantity</th>
                                                        <th>KG / Lr</th>
                                                        <th>Tax %</th>
                                                        <th>Bill-Rate</th>
                                                        <th>Total</th>
                                                        <th>sales Rate</th>
                                                        <th style="display:none"></th>                                                        
                                                        <th style="display:none"></th>
                                                        <th>Add</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="dataTable">
                                                    <tr>
                                                        <p>
                                                        <td><input class="fsze" type="checkbox" id="checkbox"  name="chk[]" checked="checked">
                                                        </td>
                                                        <td>
                                                            <input list="product_detail" name="product_detail[]" id="productdetail" class="form-control fsze" placeholder="Product Detail" style="width:280px;" required>
                                                            <datalist  name="product_detail" id="product_detail">
                                                                <option class="fsze" value="--Select Here--"></option>
                                                                @foreach($Products as $Products)
                                                                <option class="fsze" data-product_id="{{$Products->product_id}}" value="{{$Products->productname}}">
                                                                 @endforeach   
                                                            </datalist>
                                                            <span id="product_error" style="color:red;"></span>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="qty[]" id="qty" class="form-control fsze" placeholder="Qty" style="width:65px;" title="Quantity">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="measurment[]" id="measurment" class="form-control fsze" style="width:98px;" placeholder="Pcs/Kgs" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="tax[]" id="tax" class="form-control fsze" style="width:68px;" placeholder="Tax" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="purchase_rate[]" id="purchase_rate" class="form-control fsze" style="width:98px;" placeholder="Bill Rate" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="total[]" id="total" class="form-control fsze" style="width:98px;" placeholder="Total" readonly>
                                                        </td>                                                        
                                                        <td>
                                                            <input type="text" name="salerate[]" id="salerate" class="form-control fsze" style="width:98px;" placeholder="Sale rate">
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="taxrate[]" id="taxrate" class="form-control fsze" style="width:98px;" placeholder="Tax Rate" required>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="productid[]" id="productid" class="form-control " style="width:98px;" placeholder="">
                                                        </td>
                                                        <td>
                                                            <button type="button" id="addbtn1" class="btn btn-success" onClick="addRow('dataTable')"><i class="fas fa-plus"></i></button>
                                                        </td>
                                                        </p>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                            <fieldset class="row2">
                                                <table id="dataTable_extra" class="table table-bordered table-striped">
                                                    <tbody>
                                                    <tr>
                                                        <th>SubTotal</th>
                                                        <th class="bill1">IGST</th>
                                                        <th class="bill2">CGST</th>
                                                        <th class="bill2">SGST</th>
                                                        <th>Round Off</th>
                                                        <th>Other Charges</th>
                                                        <th>Total Amount</th>
                                                        <th>Paid Amount</th>
                                                        <th>Pending Amount</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" class="form-control fsze" id="subtotal" name="sub_total" style="width:110px;"  placeholder="Sub Total" title="subtotal" readonly>
                                                        </td>                                                        
                                                        <td class="bill1">
                                                            <input type="text" class="form-control fsze" id="igst" name="igst" style="width:90px;"  placeholder="IGST" title="IGST" >
                                                        </td>
                                                        <td class="bill2">
                                                            <input type="text" class="form-control fsze" id="cgst" name="cgst" style="width:90px;"  placeholder="CGST" title="CGST" >
                                                        </td>
                                                        <td class="bill2">
                                                            <input type="text" class="form-control fsze" id="sgst" name="sgst" style="width:90px;"  placeholder="SGST" title="SGST" >
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control fsze" id="round_off" name="round_off" style="width:90px;"  placeholder="Round Off" title="Round Off" >
                                                        </td>                                                        
                                                        <td>
                                                            <input type="text" class="form-control fillred fsze" id="othercharge" name="othercharge" style="width:100px;"  placeholder="Other Charges" title="Other Charges">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control fillcol fsze" id="total_amt" name="total_amt" style="width:100px;"  placeholder="Total Amt" title="Total Amount" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control fsze" id="paid" name="paid" style="width:120px;" placeholder="Paid Amt" title="Paid Amount">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control fsze" id="pending" name="pending" style="width:120px;" placeholder="Pending Amt" title="Pending Amount" readonly>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>

                                            <div class="card-footer text-right">
                                                <input  class="btn btn-success btn-lg" id="test" type="submit" name="save" value="Save">
                                                <input class="btn btn-danger btn-lg" id="test" type="reset" name="reset" value="Reset">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </section>
        </div>
        <!-- Footer Start Here -->
        @include('layouts.footer');
        <!-- Footer End Here -->
    </div>
</div>
<!-- Script Start Here -->
<!-- General JS Scripts -->
<script src="{{ asset('assets/js/app.min.js')}}"></script>
  <!-- JS Libraies -->
  <script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js')}}"></script>
  <!-- Page Specific JS File
  <script src="assets/js/page/index2.js"></script> -->
  <script src="{{ asset('assets/bundles/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{ asset('assets/bundles/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
  <!-- Template JS File -->
  <script src="{{ asset('assets/js/scripts.js')}}"></script>
  <!-- Custom JS File -->
  <script src="{{ asset('assets/js/custom.js')}}"></script>
  <script>
    function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 40){							// limit the user from creating fields more than your limits
            var row = table.insertRow(rowCount);
            var colCount = table.rows[0].cells.length;
                for(var i=0; i<colCount; i++) {
                    var newcell = row.insertCell(i);
                    newcell.innerHTML = table.rows[0].cells[i].innerHTML;
                }
        }else{
            alert("Maximum Limit is 40.");
                    
        }
    }
    function deleteRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;				
        for(var i=0; i<rowCount; i++) {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];					
            if(null != chkbox && true == chkbox.checked) {						
                if(rowCount <= 1){ 						// limit the user from removing all the fields
                    alert("Cannot Remove all the Passenger.");
                    break;
                }
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        }
    }
    $(document).ready(function(){
        $('.bill1').hide();
            $('#gst_type').change(function(){
                var gsttype = $(this).children("option:selected").val();
                if(gsttype == 'Local'){
                    $('.bill1').hide();
                    $('.bill2').show();
                }else{
                    $('.bill2').hide();
                    $('.bill1').show();
                }
                calculateSubtotal();
            });
        $("#billfrom").on('input', function () {
        //$(document).on('keyup','input[name="bill_from"]',function(){
            var dom_this = $(this);
            var val = $(this).val();
            var vid = $('#bill_from option').filter(function() {
                return this.value == val;
            }).data('vid');
            $('#vendorid').val(vid);
            $.ajax({
                type:'POST',
                url:'get_vendor',
                data:{'vid':vid , _token: '{{csrf_token()}}' },
                dataType:'json',
                success:function(data)
                {                    
                    $.each(data,function(key,value){
                        $('#vname').val(value.vname);
                        $('#mobile').val(value.mobile);
                        $('#address').val(value.address);
                        $('#gst_type option[value='+value.gst_type+']').attr('selected','selected');
                        if(value.gst_type == 'Local'){
                                $('.bill1').hide();
                                $('.bill2').show();
                            }else{
                                $('.bill2').hide();
                                $('.bill1').show();
                            }
                    });
                }
            }); 
        });
        $(document).on('change','input[name="product_detail[]"]',function(){
            var dom_this = $(this);
            var val = $(this).val();
            var rate = 0;
            var qty = 0;
            var gst = 0;
            var tax = 0;
            var total = 0;
            var product_id = $('#product_detail option').filter(function() {
                return this.value == val;
            }).data('product_id');            
            dom_this.closest('td').nextAll().eq(7).find('input').val(product_id);            
            $.ajax({
                type:'POST',
                url:'get_product',
                data:{'product_id':product_id, _token: '{{csrf_token()}}'},
                dataType:'json',
                success:function(data)
                {
                    $.each(data,function(key,value){
                        dom_this.closest('td').nextAll().eq(1).find('input').val(value.measurement);
                        dom_this.closest('td').nextAll().eq(2).find('input').val(value.gst);                        
                    });
                    rate = dom_this.closest('td').nextAll().eq(3).find('input').val();
                    qty=dom_this.closest('td').nextAll().eq(0).find('input').val();
                    gst=dom_this.closest('td').nextAll().eq(2).find('input').val();
                    total = qty * rate;
                    tax = total * gst/100;
                    dom_this.closest('td').nextAll().eq(4).find('input').val(total.toFixed(2));
                    dom_this.closest('td').nextAll().eq(6).find('input').val(tax.toFixed(2));             
                    calculateSubtotal();
                }                
            });
            
        });
        $(document).on('keyup','input[name="purchase_rate[]"]',function(){
            var rate = $(this).val();
            var qty=$(this).closest('td').prevAll().eq(2).find('input').val();
            var gst=$(this).closest('td').prevAll().eq(0).find('input').val();
            var total = qty * rate;
            var tax = total * gst/100;
            $(this).closest('td').nextAll().eq(0).find('input').val(total.toFixed(2));
            $(this).closest('td').nextAll().eq(2).find('input').val(tax.toFixed(2));             
            calculateSubtotal();
        });
        $(document).on('keyup','input[name="qty[]"]',function(){
            var qty = $(this).val();
            var rate=$(this).closest('td').nextAll().eq(2).find('input').val();
            var gst=$(this).closest('td').nextAll().eq(1).find('input').val();
            var total = qty * rate;
            var tax = total * gst/100;
            $(this).closest('td').nextAll().eq(3).find('input').val(total.toFixed(2));
            $(this).closest('td').nextAll().eq(5).find('input').val(tax.toFixed(2));             
            calculateSubtotal();
        });
        $(document).on('keyup','input[name="tax[]"]',function(){
            var gst = $(this).val();
            var rate=$(this).closest('td').nextAll().eq(0).find('input').val();
            var qty=$(this).closest('td').prevAll().eq(1).find('input').val();
            var total = qty * rate;
            var tax = total * gst/100;
            $(this).closest('td').nextAll().eq(1).find('input').val(total.toFixed(2));
            $(this).closest('td').nextAll().eq(3).find('input').val(tax.toFixed(2));             
            calculateSubtotal();
        });
        $(document).on('keyup','input[name="othercharge"]',function(){
            calculateSubtotal();
        });
        hideloop();
        $(document).on('click','button[id="addbtn1"]',function(){
                hideloop();
        });
        $(document).on('click','button[id="dlt-btn"]',function(){
            hideloop();
        });
        function hideloop(){
            $("table#datatable tr").find("input[name='chk[]']").each(function (){
                $(this).closest('td').nextAll().eq(7).hide();
                $(this).closest('td').nextAll().eq(8).hide();
            });
            return true;
        }
        function calculateSubtotal()
        {
            var subtotal=0;
            var taxtotal=0;
            var result=1;
            var igst=0;
            var cgst=0;
            var sgst=0;
            var txrate=0;
            
            $("table#datatable tr").each(function(){
                var this_input=$(this).find('input#total');
                if(this_input.val()){
                    subtotal=subtotal+parseFloat(this_input.val());
                }
            });
            $("table#datatable tr").each(function(){
                var this_input=$(this).find('input#taxrate');
                if(this_input.val()){
                    taxtotal=taxtotal+parseFloat(this_input.val());
                }
            });
            if(subtotal>0){
                result=result*subtotal;
                igst = parseFloat(parseFloat(taxtotal).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]).toFixed(2);
                cgst = parseFloat(parseFloat(taxtotal / 2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]).toFixed(2);
                sgst = parseFloat(parseFloat(taxtotal / 2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]).toFixed(2);
                var gtype = $('#gst_type').val();
                if(gtype=='Local'){                    
                    txrate = igst;
                    igst = 0;
                }else if(gtype=='Interstate'){
                    cgst=0;
                    sgst=0;
                    txrate = igst;
                }else{
                    igst = 0;
                    cgst=0;
                    sgst=0;
                    txrate = 0;
                }
                $("table#dataTable_extra tr").each(function(){
                    $(this).children("td").eq(0).find("input").val(result.toFixed(2));
                    $(this).children("td").eq(1).find("input").val(igst);
                    $(this).children("td").eq(2).find("input").val(cgst);
                    $(this).children("td").eq(3).find("input").val(sgst);

                    ground = parseFloat(result) - (parseFloat(txrate) + parseFloat(result));
                    round = Math.round(ground);
                    roundoff = parseFloat(ground) - parseFloat(round);

                    $(this).children("td").eq(4).find("input").val(roundoff.toFixed(2));
                    if($(this).children("td").eq(1).find("input").val()){
                        result=result+parseFloat($(this).children("td").eq(1).find("input").val()); 
                    }
                    if($(this).children("td").eq(2).find("input").val()){
                    result=result+parseFloat($(this).children("td").eq(2).find("input").val()); 
                    }
                    if($(this).children("td").eq(3).find("input").val()){
                    result=result+parseFloat($(this).children("td").eq(3).find("input").val()); 
                    }
                    if($(this).children("td").eq(4).find("input").val()){
                    result=result+parseFloat($(this).children("td").eq(4).find("input").val()); 
                    }                    
                    if($(this).children("td").eq(5).find("input").val()){
                    result=result+parseFloat($(this).children("td").eq(5).find("input").val()); 
                    }
                    if(result>=0){
                        $(this).children("td").eq(6).find("input").val(result.toFixed(2));
                    } 
                    var sub=0;
                    $(document).on('keyup','input[name="paid"]',function(){
                        var dom_this = $(this);
                        var paid = $(this).val();
                        sub=$(this).closest('td').prevAll().eq(0).find('input').val();
                        var totalamount=sub-paid;
                        var results = totalamount.toFixed(2);
                        if(sub>0){
                            $(this).closest('td').nextAll().eq(0).find('input').val(results);
                        }
                    });            
                });
                
            }
            return true;
        }
    });
    </script>

<!-- Script End Here -->
</body>
</html>