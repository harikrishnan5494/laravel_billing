<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">
                <img alt="image" src="{{asset('assets/img/logo.png')}}" class="header-logo" />
                <span class="logo-name">ADMIN</span>
            </a>
        </div>
        <ul class="sidebar-menu">
            <li class="dropdown active">
                <a href="{{route('dashboard')}}" class="nav-link">
                    <i class="fas fa-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-users"></i><span>Vendors</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="{{route('create_vendor')}}">Add Vendor</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{route('view_vendor')}}">View Vendor</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-users"></i><span>Customers</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="{{route('create_customer')}}">Add Customer</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{route('view_customer')}}">View Customer</a>
                    </li>
                </ul>
            </li>            
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-cubes"></i><span>Products</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="{{route('add_product')}}">Add Product</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{route('view_product')}}">View Product</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-cart-plus"></i><span>Purchase</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="{{route('add_purchase')}}">Add Purchase</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">View Purchase</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-shopping-cart"></i><span>Sales</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="#">Add Sales</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">View Sales</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-weight-hanging"></i><span>Stock</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="stock.php">View Stock</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-receipt"></i><span>Reports</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="#">Purchase Report</a>
                    </li>
                </ul>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="#">Sales Report</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-user-tie"></i><span>Employees</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="{{route('create_employee')}}">Add Employee</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{route('view_employee')}}">View Employee</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown">
                    <i class="fas fa-cogs"></i><span>Settings</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="nav-link" href="#">Profile</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Password Reset</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-forms').submit();">
                    <i class="fas fa-sign-out-alt"></i><span>Logout</span>
                </a>
                <form id="logout-forms" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </aside>
</div>
