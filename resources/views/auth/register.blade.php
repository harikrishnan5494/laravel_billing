<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
		<title>Admin Panel</title>
		<!-- General CSS Files -->
		<link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-social/bootstrap-social.css') }}">
		<!-- Template CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
		<!-- Custom style CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
		<link rel='shortcut icon' type='image/x-icon' href="{{ asset('assets/img/favicon.ico') }}" />
	</head>
    <body>
		<div class="loader"></div>
		<div id="app">
			<section class="section">
				<div class="container mt-5">
					<div class="row">
						<div class="col-4 col-sm-4 offset-sm-4 col-md-4 offset-md-4 col-lg-4 offset-lg-4 col-xl-4 offset-xl-4">
							<div class="card card-primary">
								<div class="card-header">
									<h4>Register</h4>
								</div>
								<div class="card-body">
                                    <form method="POST" action="{{ route('register') }}" class="needs-validation" novalidate="">
                                        @csrf
                                        <div class="form-group">
                                            <label for="email">Username</label>
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <div class="invalid-feedback">
                                                Please fill in your Username
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <div class="invalid-feedback">
                                                Please fill in your Email
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="control-label">Password</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror                                                
                                            <div class="invalid-feedback">
                                                    please fill in your password
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="control-label">Confirm Password</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            <div class="invalid-feedback">
                                                please fill in your confirm password
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</section>
		</div>
		<!-- General JS Scripts -->
		<script src="{{ asset('assets/js/app.min.js') }}"></script>
		<script src="{{ asset('assets/bundles/sweetalert/sweetalert.min.js') }}"></script>
		<!-- Page Specific JS File -->
		<!--<script src="assets/js/page/sweetalert.js"></script>-->
		<!-- JS Libraies -->
		<!-- Page Specific JS File -->
		<!-- Template JS File -->
		<script src="{{ asset('assets/js/scripts.js') }}"></script>
		<!-- Custom JS File -->
		<script src="{{ asset('assets/js/custom.js') }}"></script>	
	</body>
</html>

