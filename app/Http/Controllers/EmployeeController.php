<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employees;

class EmployeeController extends Controller
{
    public function create_employee(){
        return view('employees.add_employee');
    } 
    public function store_employee(Request $request){
        $Employees = new Employees;
        $Employees->emp_id = $request->emp_id;
        $Employees->empname = $request->empname;
        $Employees->empage = $request->empage;
        $Employees->empmobile = $request->empmobile;
        $Employees->empemail = $request->empemail;
        $Employees->empaadhaar = $request->empaadhaar;
        if($request->hasFile('empimage')){
            $file = $request->file('empimage');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('assets/img/employee',$filename);
            $Employees->empimage = $filename;
        }else{
            $Employees->empimage='';
        }
        $Employees->empaddress = $request->empaddress;
        $Employees->save();
        return redirect(route('create_employee'))->with('SuccessMsg','Customer Successfully Added');
    } 
    public function view_employee(){
        $Employees = Employees::all();
        return view('employees.view_employee',compact('Employees'));
    }  
    public function edit_employee($id){
        $Employees = Employees::find($id);
        return view('employees.edit_employee',compact('Employees'));
    }
    public function update_employee(Request $request, $id){
        $Employees = Employees::find($id);
        $Employees->emp_id = $request->emp_id;
        $Employees->empname = $request->empname;
        $Employees->empage = $request->empage;
        $Employees->empmobile = $request->empmobile;
        $Employees->empemail = $request->empemail;
        $Employees->empaadhaar = $request->empaadhaar;
        if($request->hasFile('empimage')){
            $file = $request->file('empimage');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('assets/img/employee',$filename);
            $Employees->empimage = $filename;
        }else{
            $Employees->empimage='';
        }
        $Employees->empaddress = $request->empaddress;
        $Employees->save();
        return redirect(route('edit_employee',$id))->with('SuccessMsg','Employee Successfully Updated');
    } 
    public function delete_employee($id){
        Employees::find($id)->delete();
        return redirect(route('view_employee'))->with('SuccessMsg','Employee deleted Successfully');
    }

}
