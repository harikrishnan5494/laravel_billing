<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
class CustomerController extends Controller
{    
    public function create_customer(){
        return view('customers.add_customer');
    }
    public function store_customer(Request $request){
        $this->validate($request,[
        'custid'=> 'required',
        'cname'=> 'required',
        'cmpname'=> 'required',
        'mobile'=> 'required',
        'email'=> 'required',
        'address'=> 'required',
        'gst_type'=> 'required'
        ]);
        $Customer = new Customers;
        $Customer->customer_id = $request->custid;
        $Customer->cname = $request->cname;
        $Customer->cmpname = $request->cmpname;
        $Customer->mobile = $request->mobile;
        $Customer->email = $request->email;
        $Customer->address = $request->address;
        $Customer->gst_type = $request->gst_type;
        $Customer->save();
        return redirect(route('create_customer'))->with('SuccessMsg','Customer Successfully Added');
    }
    public function view_customer(){
        $Customer = Customers::all();
        return view('customers.view_customer',compact('Customer'));
    }
    public function edit_customer($id){
        $Customer = Customers::find($id);
        return view('customers.edit_customer',compact('Customer'));
    }
    public function update_customer(Request $request, $id){
        $Customer = Customers::find($id);
        $Customer->customer_id = $request->custid;
        $Customer->cname = $request->cname;
        $Customer->cmpname = $request->cmpname;
        $Customer->mobile = $request->mobile;
        $Customer->email = $request->email;
        $Customer->address = $request->address;
        $Customer->gst_type = $request->gst_type;
        $Customer->save();
        return redirect(route('edit_customer',$id))->with('SuccessMsg','Customer Successfully Updated');
    }
    public function delete_customer($id){
        Customers::find($id)->delete();
        return redirect(route('view_customer'))->with('SuccessMsg','Customer deleted Successfully');
    }
}
