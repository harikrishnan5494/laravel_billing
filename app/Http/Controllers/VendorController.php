<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;

class VendorController extends Controller
{
    public function create_vendor(){
        return view('vendors.add_vendor');
    }
    public function store_vendor(Request $request){
        $this->validate($request,[
        'vendor_id'=> 'required',
        'vname'=> 'required',
        'cmpname'=> 'required',
        'mobile'=> 'required',
        'email'=> 'required',
        'address'=> 'required',
        'gst_type'=> 'required'
        ]);
        $vendor = new Vendors;
        $vendor->vendor_id = $request->vendor_id;
        $vendor->vname = $request->vname;
        $vendor->cmpname = $request->cmpname;
        $vendor->mobile = $request->mobile;
        $vendor->email = $request->email;
        $vendor->address = $request->address;
        $vendor->gst_type = $request->gst_type;
        $vendor->save();
        return redirect(route('create_vendor'))->with('SuccessMsg','Customer Successfully Added');
    }
    public function view_vendor(){
        $Vendors = Vendors::all();
        return view('vendors.view_vendor',compact('Vendors'));
    }
    public function edit_vendor($id){
        $Vendors = Vendors::find($id);
        return view('vendors.edit_vendor',compact('Vendors'));
    }
    public function update_vendor(Request $request, $id){        
        $Vendors = Vendors::find($id);
        $Vendors->vendor_id = $request->vendor_id;
        $Vendors->vname = $request->vname;
        $Vendors->cmpname = $request->cmpname;
        $Vendors->mobile = $request->mobile;
        $Vendors->email = $request->email;
        $Vendors->address = $request->address;        
        $Vendors->gst_type = $request->gst_type;
        $Vendors->save();
        return redirect(route('edit_vendor',$id))->with('SuccessMsg','Customer Successfully Updated');
    }
    public function delete_vendor($id){
        Vendors::find($id)->delete();
        return redirect(route('view_vendor'))->with('SuccessMsg','Customer deleted Successfully');
    }
}
