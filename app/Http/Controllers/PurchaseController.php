<?php

namespace App\Http\Controllers;
use App\Vendors;
use App\Products;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    public function add_purchase(){
        $Vendors = Vendors::all();
        $Products = Products::all();
        return view('purchase.add_purchase',['Vendors' => $Vendors,'Products'=>$Products]);
    }
}
