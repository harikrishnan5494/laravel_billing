<?php

namespace App\Http\Controllers;
use App\Customers;
use App\Vendors;
use App\Products;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function get_vendor(Request $request){
        $vid = $request->vid;
        $Vendors =  Vendors::select("*")
                    ->where('vendor_id', "=", $vid)
                    ->get(['vmobile','address','gst_type']);
        return json_encode($Vendors);
    }
    public function get_product(Request $request){
        $product_id = $request->product_id;
        $Products =  Products::select("*")
                    ->where('product_id', "=", $product_id)
                    ->get();
        return json_encode($Products);
    }
}
