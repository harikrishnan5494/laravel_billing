<?php

namespace App\Http\Controllers;
use App\Products;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function add_product(){
        $Products = Products::max('product_id');
        $product_id = (int)$Products+1;
        return view('products.add_product',array('product_id'=>$product_id));
    }
    public function store_product(Request $request){

        $productname = implode(",",$request->productname);
        $productid = implode(",",$request->productid);
        $measurement = implode(",",$request->measurement);
        $hsncode = implode(",",$request->hsncode);
        $gst = implode(",",$request->gst);
        $salerate = implode(",",$request->salerate);

        $pname = explode(",",$productname);
        $pid = explode(",",$productid);
        $unit = explode(",",$measurement);
        $hsn = explode(",",$hsncode);
        $tax = explode(",",$gst);
        $rate = explode(",",$salerate);

        $count=count($pname);

        for($i=0;$i<$count;$i++){
            $Products = new Products;
            $Products->product_id = $pid[$i];
            $Products->productname = $pname[$i];
            $Products->measurement = $unit[$i];
            $Products->hsncode = $hsn[$i];
            $Products->gst = $tax[$i];
            $Products->rate = $rate[$i];
            $Products->save();
        }        
        return redirect(route('add_product'))->with('SuccessMsg','Product Successfully Added');
    }    
    public function view_product(){
        $Products = Products::all();
        return view('products.view_product',compact('Products'));
    }
    public function edit_product($id){
        $Products = Products::find($id);
        return view('products.edit_product',compact('Products'));
    }
    public function update_product(Request $request, $id){
        $Products = Products::find($id);
        $Products->product_id = $request->productid;
        $Products->productname = $request->productname;
        $Products->measurement = $request->measurement;
        $Products->hsncode = $request->hsncode;
        $Products->gst = $request->gst;
        $Products->rate = $request->salerate;
        $Products->save();
        return redirect(route('edit_product',$id))->with('SuccessMsg','Customer Successfully Updated');
    }
    public function delete_product($id){
        Products::find($id)->delete();
        return redirect(route('view_product'))->with('SuccessMsg','Product deleted Successfully');
    }
}
